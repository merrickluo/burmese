//
//  AppDelegate.swift
//  burmese
//
//  Created by Merrick Luo on 4/6/19.
//  Copyright © 2019 Merrick Luo. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    private var statusItem: NSStatusItem? = nil
    
    var moveable = true
    
    @objc private func toggleMove() {
        self.moveable = !self.moveable
        NSApp.windows.first?.ignoresMouseEvents = !self.moveable
    }
    
    @objc private func onFocusWindowChange() {
        print("focus window change")
    }
    
    @objc private func onSpaceChange(noti: NSNotification) {
        print("changing space")
        return
    }
    
    @objc private func onAppLaunched(noti: Notification) {
        print("app launch")
    }
    
    @objc private func onAppTerminated(noti: Notification) {
        print("app terminated")
    }
    
    private func addMenuBarIcon() {
        statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
        
        if let button = statusItem?.button {
            button.image = NSImage(named: "StatusBarButtonImage")
        }
        
        let menu = NSMenu()
        menu.addItem(NSMenuItem(
            title: "Toggle Move",
            action: #selector(toggleMove),
            keyEquivalent: "m"
        ))
        
        menu.addItem(NSMenuItem.separator())
        
        menu.addItem(NSMenuItem(
            title: "Quit",
            action: #selector(NSApplication.terminate(_:)),
            keyEquivalent: "q"
        ))

        statusItem?.menu = menu
    }
    
    func registerForAppSwitchNotification(app: NSRunningApplication) {
        //print(app.processIdentifier)
    }
    
    func setupNotificationReceiver() {
        let nc = NSWorkspace.shared.notificationCenter

        addObserver(self, forKeyPath: #keyPath(NSScreen.main), options: [.old, .new], context: nil)
        
        // application launch
        nc.addObserver(
            self,
            selector: #selector(onAppLaunched),
            name: NSWorkspace.didLaunchApplicationNotification,
            object: NSWorkspace.shared
        )
        
        // application launch
        nc.addObserver(
            self,
            selector: #selector(onAppTerminated),
            name: NSWorkspace.didTerminateApplicationNotification,
            object: NSWorkspace.shared
        )
        
        for app in NSWorkspace.shared.runningApplications {
            registerForAppSwitchNotification(app: app)
        }
    }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        NSApp.setActivationPolicy(.accessory)
        addMenuBarIcon()
        setupNotificationReceiver()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}
