//
//  MoveableImageView.swift
//  burmese
//
//  Created by Merrick Luo on 4/6/19.
//  Copyright © 2019 Merrick Luo. All rights reserved.
//

import Cocoa

class MoveableImageView: NSImageView {

    override var mouseDownCanMoveWindow: Bool {
        return true
    }
}
