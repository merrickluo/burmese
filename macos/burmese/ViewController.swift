//
//  ViewController.swift
//  burmese
//
//  Created by Merrick Luo on 4/6/19.
//  Copyright © 2019 Merrick Luo. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var imgView: NSImageView!

    private var currentCat = 0
    
    private func loadImages() -> Array<NSImage?> {
        return (1...20).map { (i) in
            return NSImage(named: "cat\(i)")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let images = loadImages()

        let layer = CALayer()
        let animation = CAKeyframeAnimation(keyPath: "contents")
        animation.calculationMode = .discrete
        animation.duration = 2
        animation.repeatCount = .infinity
        animation.values = images as [Any]

        layer.frame = self.imgView.frame
        layer.bounds = self.imgView.bounds
        layer.minificationFilter = .linear
        layer.add(animation, forKey: "contents")
        self.imgView.layer = layer
        self.imgView.wantsLayer = true
        self.imgView.alphaValue = 0.8
    }
}
