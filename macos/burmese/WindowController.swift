//
//  WindowController.swift
//  burmese
//
//  Created by Merrick Luo on 4/6/19.
//  Copyright © 2019 Merrick Luo. All rights reserved.
//

import Cocoa

class WindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        self.window?.isOpaque = false
        self.window?.backgroundColor = NSColor(red: 1, green: 0.5, blue: 0.5, alpha: 0)
        self.window?.isMovableByWindowBackground = true
        self.window?.titleVisibility = .hidden
        self.window?.titlebarAppearsTransparent = true
        
        self.window?.styleMask = .borderless
        self.window?.level = .floating
        
        self.window?.collectionBehavior = .canJoinAllSpaces
        self.window?.level = .screenSaver
        self.window?.hasShadow = false

        self.window?.setFrameAutosaveName("burmese");
    }
}
