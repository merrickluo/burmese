const std = @import("std");
const sdl = @import("sdl.zig");

pub const Render = struct {
    // skip scale if it's 1.0
    const no_scale = 1.0;

    // compile image binaries to executable
    const images = [21][]const u8{
        @embedFile("../assets/cat1.svg"),
        @embedFile("../assets/cat2.svg"),
        @embedFile("../assets/cat3.svg"),
        @embedFile("../assets/cat4.svg"),
        @embedFile("../assets/cat5.svg"),
        @embedFile("../assets/cat6.svg"),
        @embedFile("../assets/cat7.svg"),
        @embedFile("../assets/cat8.svg"),
        @embedFile("../assets/cat9.svg"),
        @embedFile("../assets/cat10.svg"),
        @embedFile("../assets/cat11.svg"),
        @embedFile("../assets/cat12.svg"),
        @embedFile("../assets/cat13.svg"),
        @embedFile("../assets/cat14.svg"),
        @embedFile("../assets/cat15.svg"),
        @embedFile("../assets/cat16.svg"),
        @embedFile("../assets/cat17.svg"),
        @embedFile("../assets/cat18.svg"),
        @embedFile("../assets/cat19.svg"),
        @embedFile("../assets/cat20.svg"),
        @embedFile("../assets/cat21.svg"),
    };

    // window & image dimension.
    dimension: sdl.c.SDL_Rect = .{ .h = 0, .w = 0, .x = 0, .y = 0 },

    // How window shape be made from image.
    shape_mode: sdl.c.SDL_WindowShapeMode = .{
        .mode = sdl.c.ShapeModeBinarizeAlpha,
        .parameters = .{ .binarizationCutoff = 100 }
    },

    // load image surfaces to memory
    surfaces: [21]*sdl.c.SDL_Surface,
    // load image textures to memory, lazy init in render
    textures: [21]?*sdl.c.SDL_Texture,

    pub fn new(scale: f32) Render {
        var surfaces: [21]*sdl.c.SDL_Surface = undefined;
        var textures: [21]?*sdl.c.SDL_Texture = undefined;
        for (images) |image, i| {
            const unscaled_surface = sdl.LoadImageBinary(image);
            if (scale != no_scale) {
                surfaces[i] = sdl.c.rotozoomSurface(unscaled_surface, 0.0, scale, sdl.c.SMOOTHING_ON);
                _ = sdl.c.SDL_FreeSurface(unscaled_surface);
            } else {
                surfaces[i] = unscaled_surface;
            }
            textures[i] = null;
        }

        return Render{
            .surfaces = surfaces,
            .textures = textures,
        };
    }

    pub fn free(self: *Render) void {
        for (self.surfaces) |surface| {
            sdl.c.SDL_FreeSurface(surface);
        }

        for (self.textures) |texture| {
            if (texture != null) {
                sdl.c.SDL_DestroyTexture(texture);
            }
        }
    }

    pub fn render(self: *Render, renderer: *sdl.c.SDL_Renderer, window: *sdl.c.SDL_Window, id: i32) !void {
        const idx = @intCast(usize, id);
        const surface = self.surfaces[idx];
        var texture = self.textures[idx];
        if (texture == null) {
            texture = sdl.c.SDL_CreateTextureFromSurface(renderer, surface);
            self.textures[idx] = texture;
        }

        _ = sdl.c.SDL_QueryTexture(texture, null, null, &self.dimension.w, &self.dimension.h);
        _ = sdl.c.SDL_SetWindowSize(window, self.dimension.w, self.dimension.h);
        _ = sdl.c.SDL_SetWindowShape(window, surface, &self.shape_mode);

        _ = sdl.c.SDL_RenderClear(renderer);
        _ = sdl.c.SDL_RenderCopy(renderer, texture, &self.dimension, &self.dimension);
        sdl.c.SDL_RenderPresent(renderer);
    }
};
