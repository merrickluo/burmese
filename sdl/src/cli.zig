const std = @import("std");

const scale_tag = "--scale";
const opacity_tag = "--opacity";
const moveable_tag = "--moveable";

// Simple cli arg parse just for cats.
pub const Parser = struct {
    scale: ?f32 = null,
    opacity: ?f32 = null,
    moveable: ?bool = null,

    pub fn new() Parser {
        return Parser{};
    }

    pub fn parse(self: *Parser) !void {
        var args = std.process.args();
        _ = args.skip(); // skip program name
        while (args.nextPosix()) |tag| {
            if (tag.len >= scale_tag.len and std.mem.eql(u8, tag, scale_tag)) {
                if (args.nextPosix()) |value| {
                    self.scale = try std.fmt.parseFloat(f32, value);
                } else {
                    return error.InvalidArguments;
                }
            } else if (tag.len >= opacity_tag.len and std.mem.eql(u8, tag, opacity_tag)) {
                if (args.nextPosix()) |value| {
                    self.opacity = try std.fmt.parseFloat(f32, value);
                } else {
                    return error.InvalidArguments;
                }
            } else if (tag.len >= moveable_tag.len and std.mem.eql(u8, tag, moveable_tag)) {
                if (args.nextPosix()) |value| {
                    self.moveable = std.mem.eql(u8, value, "true");
                } else {
                    return error.InvalidArguments;
                }
            }
        }
    }
};
