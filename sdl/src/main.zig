const std = @import("std");
const builtin = @import("builtin");
const sdl = @import("sdl.zig");
const cat = @import("cat.zig");
const cli = @import("cli.zig");

const Timer = std.time.Timer;

// Application settings.
const Application = struct {
    const time_per_frame: u64 = 100 * std.time.ns_per_ms; // 10 frame per second.

    scale: f32 = 1.0,
    opacity: f32 = 0.8,
    moveable: bool = true,

    current: i32 = 0,
    quit: bool = false,
    timer: Timer = undefined,
    frame_time: i128 = time_per_frame,
    cat_render: ?*cat.Render = null,

    pub fn maybeNext(self: *Application) bool {
        const diff = self.frame_time - self.timer.lap();
        if (diff <= 0) {
            self.current += 1;
            if (self.current > 20) {
                self.current = 0;
            }
            self.frame_time = time_per_frame + diff;
            return true;
        }
        self.frame_time = diff;
        return false;
    }
};

var app = Application{};
pub fn main() !void {
    if (sdl.c.SDL_Init(sdl.c.SDL_INIT_VIDEO) != 0) {
        sdl.c.SDL_Log("Unable to initialize SDL.c: %s", sdl.c.SDL_GetError());
        return error.SDLInitializationFailed;
    }
    defer sdl.c.SDL_Quit();

    const initial_w: i32 = 10;
    const screen = sdl.c.SDL_CreateShapedWindow("burmese", 100, 100, initial_w, 0, sdl.c.SDL_WINDOW_OPENGL | sdl.c.SDL_WINDOW_ALLOW_HIGHDPI) orelse {
        sdl.c.SDL_Log("Unable to create window: %s", sdl.c.SDL_GetError());
        return error.SDLInitializationFailed;
    };
    defer sdl.c.SDL_DestroyWindow(screen);

    _ = sdl.c.SDL_SetWindowOpacity(screen, app.opacity);
    _ = sdl.c.SDL_SetWindowHitTest(screen, moveableCallback, null);
    const renderer = sdl.c.SDL_CreateRenderer(screen, -1, sdl.c.SDL_RENDERER_ACCELERATED) orelse {
        sdl.c.SDL_Log("Unable to create renderer: %s", sdl.c.SDL_GetError());
        return error.SDLInitializationFailed;
    };
    defer sdl.c.SDL_DestroyRenderer(renderer);
    _ = sdl.c.SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);

    const args = &cli.Parser.new();
    try args.parse();

    if (args.opacity) |op| {
        app.opacity = op;
    }
    if (args.moveable) |ma| {
        app.moveable = ma;
    }

    if (args.scale) |sa| {
        app.scale = sa;
    } else {
        // calcuate scale factor based on drawable size / window size;
        // only if scale is not passed through cli args.
        var w: i32 = 0;
        _ = sdl.c.SDL_GL_GetDrawableSize(screen, &w, null);
        app.scale = @intToFloat(f32, w) / @intToFloat(f32, initial_w);
    }

    app.timer = try Timer.start();
    app.cat_render = &cat.Render.new(app.scale);
    defer app.cat_render.?.free();

    while (!app.quit) {
        var event: sdl.c.SDL_Event = undefined;
        while (sdl.c.SDL_PollEvent(&event) != 0) {
            switch (event.@"type") {
                sdl.c.SDL_QUIT => {
                    app.quit = true;
                },
                sdl.c.SDL_KEYDOWN => {
                    const key = event.@"key";
                    const meta_hold = key.keysym.mod & sdl.c.KMOD_GUI != 0;
                    if (meta_hold and key.keysym.sym == sdl.c.SDLK_m) {
                        app.moveable = !app.moveable;
                    }
                },
                else => {},
            }
        }
        if (app.maybeNext()) {
            try app.cat_render.?.render(renderer, screen, app.current);
        }
    }
}

fn moveableCallback(w: ?*sdl.c.SDL_Window, area: [*c]const sdl.c.SDL_Point, data: ?*c_void) callconv(.C) sdl.c.SDL_HitTestResult {
    _ = w;
    _ = area;
    _ = data;

    return if (app.moveable) sdl.c.SDL_HITTEST_DRAGGABLE else sdl.c.SDL_HITTEST_NORMAL;
}
