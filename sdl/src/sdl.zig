const std = @import("std");
pub const c = @cImport({
    @cInclude("SDL.h");
    @cInclude("SDL_image.h");
    @cInclude("SDL2_rotozoom.h");
});

pub fn LoadImageBinary(binary: []const u8) *c.SDL_Surface {
    var buf: [2048]u8 = undefined;
    std.mem.copy(u8, &buf, binary);
    const ops = c.SDL_RWFromMem(@ptrCast(*c_void, &buf), @intCast(c_int, binary.len));
    return c.IMG_Load_RW(ops, 1);
}
